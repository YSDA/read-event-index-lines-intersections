#!/usr/bin/env python3
from itertools import chain
from collections import defaultdict
import argparse


def parse_intersection(text_line):
    """Parses the line in format <stripping_version>.<line1>.<line2>\t<overlap>\n,
    returns (stripping_version, (frozenset((line_one, line_two)),
    int(overlap)))"""
    intersection_id, overlap = text_line.strip().split("\t")
    stripping_version, line_one, line_two = intersection_id.split(".")
    return (stripping_version, (frozenset((line_one, line_two)), int(overlap)))


def read_lines_intersections(io_object):
    """Reads the intersections file into a dictionary.
    File format:
    <stripping_version>.<line1>.<line2>\t<overlap>\n
    Must contain entries <line1>.<line1> with the total counts.
    Missing pairs are assumed to have zero overlap.
    Args:
       io_object: iterator to read the text lines
    Returns:
       {<stripping_version>: {frozenset(<line1>, <line2>): overlap, ...}, ...}
    """
    overlaps = defaultdict(dict)
    for stripping_version, record in map(parse_intersection, io_object):
        overlaps[stripping_version][record[0]] = record[1]
    # Do the sanity checks
    # All the counts should be non-zero
    min_overlap = min(map(
        lambda overlaps_for_version: min(overlaps_for_version.values()),
        overlaps.values()))
    if min_overlap < 0:
        raise ValueError("Negative overlap encountered")
    elif min_overlap == 0:
        print("Warning: overlap 0 encountered. In "
              "principle it's valid, but MapReduce "
              "shouldn't have produced it")
    # Check that every line intersects with itself no less than
    # with the others
    for stripping_version, overlaps_for_version in overlaps.items():
        lines = frozenset(chain(*overlaps_for_version.keys()))
        lines_counts = dict(((line, overlaps_for_version[
            frozenset((line, line))]) for line in lines))
        for line_pair, overlap_for_pair in overlaps_for_version.items():
            for line in line_pair:
                if lines_counts[line] < overlap_for_pair:
                    raise ValueError("Total count for line %s is less "
                                     "than the total count for pair (%s, %s)"
                                     ", stripping version %s" % (
                                         line, *line_pair, stripping_version))
    return overlaps


def main():
    parser = argparse.ArgumentParser(
        description="Verify lines intersection file")
    parser.add_argument("input_file", type=str)
    args = parser.parse_args()
    with open(args.input_file) as input_io:
        overlaps = read_lines_intersections(input_io)
    print("Valid file %s" % args.input_file)
    for stripping_version, overlaps_for_version in overlaps.items():
        print("Stripping %s has %d records" % (
            stripping_version, len(overlaps_for_version)))

if __name__ == "__main__":
    main()
